#!/usr/bin/python3
# pylint: disable=invalid-name
# pylint: disable=too-many-locals
# pylint: disable=too-many-branches
# pylint: disable=too-many-statements
# pylint: disable=too-many-arguments
# pylint: disable=too-many-nested-blocks

"""Script to inform users of a distributions end of life"""
import argparse
import time
import sys
import pprint
import socket
from datetime import datetime, timedelta
from urllib.parse import urlencode

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import requests
import opensearchpy
from opensearchpy import OpenSearch, RequestsHttpConnection
from requests_kerberos import HTTPKerberosAuth, OPTIONAL

def convert_date(date_string):
    """convert DD.MM.YYYY to something nicer for humans"""
    date_obj = datetime.strptime(date_string, "%d.%m.%Y")
    day = date_obj.day
    month = date_obj.strftime("%B")  # Full month name
    year = date_obj.year
    if 4 <= day <= 20 or 24 <= day <= 30:
        suffix = "th"
    else:
        suffix = ["st", "nd", "rd"][day % 10 - 1]
    return f"{day}{suffix} of {month} {year}"

def get_pdb_request(url, query, debug):
    """query puppetdb"""
    url_suffix = 'https://constable.cern.ch:9081'
    query_dict = {}
    if query is not None:
        query_dict["query"]=query
    full_url = f"{url_suffix}/{url}?{urlencode(query_dict)}"
    auth = HTTPKerberosAuth()
    retries = 2
    for _ in range(retries):
        pdb_data = requests.get(full_url, auth=auth)
        if pdb_data.status_code != 200:
            if debug:
                print(
                    f"Puppetdb status code was {pdb_data.status_code}"
                    f" with query: {query}. Retrying ..."
                )
        else:
            return pdb_data.json()
    if debug:
        print(
            f"Puppetdb call failed after {retries} retries"
            f" for query: {query}. Returning null for this request ..."
        )
    return {}

def parse_args():
    """define command line arguments"""
    aparser = argparse.ArgumentParser(
        description='Get statistics about CERN hosts accessing linuxsoft repositories'
    )
    aparser.add_argument(
        '--ostag',
        help='OS tag to search for',
        action='store',
        dest='ostag',
    )
    aparser.add_argument(
        '--days',
        help='number of days',
        default=1,
        action='store',
        dest='days',
        type=int,
    )
    aparser.add_argument(
        '--maxrecords',
        help='optional maxrecords to return, useful for debugging',
        action='store',
        dest='maxrecords',
        type=int,
    )
    aparser.add_argument(
        '--override_to',
        help='override the to email, useful for testing',
        default=False,
        action='store',
        dest='override_to',
    )
    aparser.add_argument(
        '--type',
        help='query puppet,nonpuppet or all types of hosts',
        default='all',
        action='store',
        dest='hosttype',
    )
    aparser.add_argument(
        '--email',
        help='generate a template to send an email',
        action='store_true',
        default=False,
        dest='email',
    )
    aparser.add_argument(
        '--owner',
        help='restrict query to one or more email addresses (whitespace separated)',
        action='store',
        nargs='+',
        dest='owner',
    )
    aparser.add_argument(
        '--department',
        help='restrict query to one or more specific departments (whitespace separated)',
        action='store',
        nargs='+',
        dest='department',
    )
    aparser.add_argument(
        '--group',
        help='restrict query to a specific group (requires department)',
        action='store',
        dest='group',
    )
    aparser.add_argument(
        '--exclude_department',
        help='a list of departments to exclude (whitespace separated)',
        action='store',
        nargs='+',
        dest='exclude_department',
    )
    aparser.add_argument(
        '--onlydc',
        help='Only include systems that reside in a datacentre)',
        action='store_true',
        default=False,
        dest='onlydc',
    )
    aparser.add_argument(
        '--pretty',
        help='pretty print some additional stats',
        action='store_true',
        dest='pretty',
    )
    aparser.add_argument(
        '--debug',
        help='print debug messages where appropriate',
        action='store_true',
        dest='debug',
    )
    aparser.add_argument(
        '--emailfrom',
        help='If sending an email, who are we sending it as? default: Linux.Support@cern.ch',
        action='store',
        default='Linux.Support@cern.ch',
        dest='email_from',
    )
    return aparser.parse_args()


def setup_query(
    ostag, puppetmanaged, department, group, owner,
    days, maxrecords, onlydc, hostname=None
    ):
    """setup the query from the template"""
    endtime = datetime.today()
    starttime = endtime - timedelta(days=days)
    stime = starttime.strftime("%Y-%m-%dT%H:%M:%S")
    etime = endtime.strftime("%Y-%m-%dT%H:%M:%S")

    filter_list = []
    if ostag is not None:
        OSTAG = {'match_phrase': {'OS': ostag}}
        filter_list.append(OSTAG)
    RANGE = {
        'range': {
            '@timestamp': {
                'gte': stime,
                'lte': etime,
                'format': "yyyy-MM-dd'T'HH:mm:ss",
            }
        }
    }
    filter_list.append(RANGE)
    if 'all' not in puppetmanaged:
        PUPPET_MANAGED = {'match_phrase': {'PuppetManaged': puppetmanaged}}
        filter_list.append(PUPPET_MANAGED)
    if 'all' not in department:
        # Multiple departments have been passed
        deps = []
        for dep in department:
            deps.append({'match_phrase': {'Department': dep}})
        DEPARTMENT = {
            'bool': {
                'should': deps,
                'minimum_should_match': 1,
            }
        }
        filter_list.append(DEPARTMENT)
    if 'all' not in group:
        GROUP = {'match_phrase': {'Group': group}}
        filter_list.append(GROUP)
    if 'all' not in owner:
        # Multiple owners have been passed
        owners = []
        for own in owner:
            owners.append({'match_phrase': {'Owner': own}})
        OWNER = {
            'bool': {
                'should': owners,
                'minimum_should_match': 1,
            }
        }
        filter_list.append(OWNER)
    if onlydc:
        dc = []
        datacentres = [
          '0000-0-0000',
          '0513-S-0034',
          '0513-R-0050',
          '0513-R-0060',
          '0613-R-0001',
          '0773-R-0402',
          '0775-2-0001',
        ]
        for datacentre in datacentres:
            dc.append({'match_phrase': {'Location': datacentre}})
        LOCATION = {
            'bool': {
                'should': dc,
                'minimum_should_match': 1,
            }
        }
        filter_list.append(LOCATION)
    if hostname is not None:
        HOSTNAME = { "term": { "Hostname": hostname }}
        filter_list.append(HOSTNAME)

    query = {
        'version': True,
        'size': maxrecords,
        '_source': [
            '@timestamp',
            'Department',
            'Group',
            'Hostname',
            'Location',
            'OS',
            'Owner',
            'PuppetManaged',
            'Requests',
        ],
        'query': {
            'bool': {
                # It is actually necessary ;)
                # pylint: disable=unnecessary-comprehension
                'filter': [item for item in filter_list],
            },
        },
    }
    return query

def get_data_from_os(client, query, maxrecords, scroll=True):
    """Get data from Opensearch"""
    indexname = 'linux_private-lxsoft-stats*'
    if not scroll:
        try:
            iterator = client.search(body=query, index=indexname)
        except opensearchpy.exceptions.AuthenticationException:
            print("Failed to auth, try kinit?")
            sys.exit(1)
        for _hit in iterator['hits']['hits']:
            yield _hit
    else:
        try:
            search_result = client.search(
                body=query,
                index=indexname,
                size=maxrecords,
                scroll="3m",
                timeout=30
            )
        except opensearchpy.exceptions.AuthenticationException:
            print("Failed to auth, try kinit?")
            sys.exit(1)
        scroll_id = search_result['_scroll_id']
        scroll_size = len(search_result['hits']['hits'])  # this is the current 'page' size
        counter = 0
        for _hit in search_result['hits']['hits']:
            yield _hit
        if maxrecords != 10000:
            return

        while scroll_size > 0:
            counter += scroll_size
            scroll_result = client.scroll(scroll_id=scroll_id, scroll="1s", timeout=30)
            scroll_id = scroll_result['_scroll_id']
            scroll_size = len(scroll_result['hits']['hits'])
            for _hit in scroll_result['hits']['hits']:
                yield _hit

def main():
    """main entry point"""

    # get command line arguments
    args = parse_args()
    days = args.days
    email = args.email
    ostag = args.ostag
    if ostag is None:
        print("No --ostag passed, exiting")
        sys.exit(1)
    hosttype = args.hosttype
    pretty = args.pretty
    debug = args.debug
    if 'nonpuppet' in hosttype:
        puppetmanaged = 'false'
    elif 'puppet' in hosttype:
        puppetmanaged = 'true'
    # all
    else:
        puppetmanaged = 'all'
    if args.maxrecords:
        maxrecords = args.maxrecords
        print(f"INFO: maxrecords:{maxrecords} passed")
    else:
        maxrecords = 10000

    if args.department:
        department = args.department
    else:
        department = 'all'

    if args.group:
        if 'all' in department:
            print("Please also pass --department")
            sys.exit(1)
        group = args.group
    else:
        group = 'all'
    if args.exclude_department:
        exclude_department = args.exclude_department
    else:
        exclude_department = None
    if args.owner:
        owner = args.owner
    else:
        owner = 'all'
    onlydc = args.onlydc
    email_from = args.email_from
    override_to = args.override_to

    similar_distribution = 'RedHat'
    distribution_short_name = ostag.upper()
    dashboard_url = "https://monit-grafana.cern.ch/d/zkmdcVTSz/distributions-stats?orgId=18"
    if 'slc6' in ostag:
        distribution_full_name = 'Scientific Linux CERN 6'
        eol="30.11.2020"
    elif 'cc7' in ostag:
        distribution_full_name = 'CERN CentOS 7'
        eol="30.06.2024"
        # pylint: disable=line-too-long
        dashboard_url = "https://monit-grafana.cern.ch/d/ab405102-89a9-41ad-b3d4-9903b4a01153/rhel-cc-7-deprecation?orgId=18"
    elif 'rhel7' in ostag:
        distribution_full_name = 'Red Hat Enterprise Linux 7'
        similar_distribution = 'CentOS'
        eol="30.06.2024"
        # pylint: disable=line-too-long
        dashboard_url = "https://monit-grafana.cern.ch/d/ab405102-89a9-41ad-b3d4-9903b4a01153/rhel-cc-7-deprecation?orgId=18"
    elif 'c8' in ostag:
        distribution_full_name = 'CentOS Linux 8'
        eol="31.12.2021"
    elif 'cs8' in ostag:
        distribution_full_name = 'CentOS Stream 8'
        eol="30.09.2023"
    elif 'cs9' in ostag:
        distribution_full_name = 'CentOS Stream 9'
        eol="30.06.2023"
    elif 'rhel8' in ostag:
        distribution_full_name = 'Red Hat Enterprise Linux 8'
        eol="31.05.2029"
    elif 'rhel9' in ostag:
        distribution_full_name = 'Red Hat Enterprise Linux 9'
        eol="31.05.2032"
    elif 'alma8' in ostag:
        distribution_full_name = 'AlmaLinux 8'
        eol="31.05.2029"
    elif 'alma9' in ostag:
        distribution_full_name = 'AlmaLinux 9'
        eol="31.05.2032"
    else:
        print("I can't handle anything else at this point. Panic!")
        sys.exit(1)

    eol_human = convert_date(eol)

    print(
        "INFO: running query with ["
        f"ostag: {ostag}, days: {days}, puppetmanaged: {puppetmanaged},"
        f" department: {department}, group: {group}, owner: {owner}]"
    )

    client = OpenSearch(
        ['https://os-linux.cern.ch/os'],
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection,
        http_auth=HTTPKerberosAuth(mutual_authentication=OPTIONAL),
    )
    query = setup_query(
            ostag, puppetmanaged, department, group, owner,
            days, maxrecords, onlydc, None
            )
    cern_dict = {}
    for hit in get_data_from_os(client, query, maxrecords):
        server = hit['_source']['Hostname']
        reqs = hit['_source']['Requests']
        if exclude_department is not None:
            if hit['_source']['Department'] in exclude_department:
                continue
        if server in cern_dict:
            totalrequests = int(cern_dict[server]['Requests'] + reqs)
            cern_dict[server]['Requests'] = totalrequests
        else:
            cern_dict[server] = {
                'Requests': reqs,
                'Department': hit['_source']['Department'],
                'Group': hit['_source']['Group'],
                'Owner': hit['_source']['Owner'],
                'PuppetManaged': hit['_source']['PuppetManaged'],
            }
    print(f"QUERY: {len(cern_dict)} raw total number of hosts, before exclusions applied")
    # Checking if the host still exists, no point sending an email for a decommmissioned machine
    # Also, to reduce socket lookups - take the opportunity to add puppet managed info if we have it
    to_remove = []
    ats_hosts = 0
    deleted_hosts = 0
    puppet_upgraded_hosts = 0
    unmanaged_upgraded_hosts = 0
    for key in cern_dict.items():
        exists_in_DNS = False
        #pylint: disable=unnecessary-dict-index-lookup
        if cern_dict[key[0]]['Owner'] is None:
            to_remove.append(key[0])
            continue
        try:
            socket.gethostbyname(key[0])
            exists_in_DNS = True
        except socket.gaierror:
            # try ipv6 as well
            try:
                socket.getaddrinfo(key[0], None, socket.AF_INET6)
                exists_in_DNS = True
            except socket.gaierror:
                pass
        if not exists_in_DNS:
            to_remove.append(key[0])
            deleted_hosts += 1
        else:
            # exclude ATS machines (for cc7)
            if 'cc7' in ostag:
                for acc in ['ACC-console-responsible@cern.ch', 'ACC-adm@cern.ch' ]:
                    #pylint: disable=unnecessary-dict-index-lookup
                    if cern_dict[key[0]]['Owner'] is not None:
                        #pylint: disable=unnecessary-dict-index-lookup
                        if acc in cern_dict[key[0]]['Owner']:
                            if debug:
                                print(f"Removing {key[0]} as it's an ATS machine")
                            to_remove.append(key[0])
                            ats_hosts += 1
                            continue
            #pylint: disable=unnecessary-dict-index-lookup
            if 'true' in cern_dict[key[0]]['PuppetManaged']:
                puppetdb_req = get_pdb_request(
                    '/pdb/query/v4/facts/os',
                    f'["=","certname","{key[0]}.cern.ch"]',
                    debug
                )
                # Host has been ai-rebuild to something other than original ostag
                if len(puppetdb_req) > 0:
                    if similar_distribution in puppetdb_req[0]['value']['name']:
                        if debug:
                            print(
                                f"Removing {key[0]} as puppetdb reports the"
                                " osname as "
                                f"{puppetdb_req[0]['value']['name']}"
                            )
                        to_remove.append(key[0])
                        puppet_upgraded_hosts += 1
                    elif ostag[-1] not in puppetdb_req[0]['value']['release']['major']:
                        if debug:
                            print(
                                f"Removing {key[0]} as puppetdb reports the"
                                " osmajor as "
                                f"{puppetdb_req[0]['value']['release']['major']}"
                            )
                        to_remove.append(key[0])
                        puppet_upgraded_hosts += 1
    for remove in to_remove:
        cern_dict.pop(remove, None)
    # reset variable
    to_remove = []
    for key in cern_dict.items():
        query = setup_query(
                None, puppetmanaged, department, group, owner,
                days, maxrecords, onlydc, key[0]
                )
        # query all ostags for the server, perhaps it's been upgraded already
        # this is slower, but improves our hit rate
        for hit in get_data_from_os(client, query, maxrecords, scroll=False):
            # eg "rhel9" is greater than "cc7"
            if hit['_source']['OS'][-1] > ostag[-1]:
                if debug:
                    print(
                        f"Removing {key[0]} as it's also reported as "
                        f"{hit['_source']['OS']}, which is greater than than {ostag}"
                    )
                to_remove.append(key[0])
                unmanaged_upgraded_hosts += 1
                break
    for remove in to_remove:
        cern_dict.pop(remove, None)

    if 'cc7' in ostag:
        print(f"QUERY: excluded {ats_hosts} ATS hosts")
    print(f"QUERY: excluded {deleted_hosts} hosts removed from DNS")
    print(f"QUERY: excluded {puppet_upgraded_hosts} puppet hosts that have since upgraded")
    print(f"QUERY: excluded {unmanaged_upgraded_hosts} unmanaged hosts that have since upgraded")
    print(
        f"RESULT: There are {len(cern_dict)} {ostag} hosts that have accessed"
        f" linuxsoft in the past {days} days"
    )
    puppet = 0
    for item in cern_dict.items():
        if 'true' in item[1]['PuppetManaged']:
            puppet+= 1
    print(
          f"RESULT: Of which {puppet} are puppet managed"
          f", and {len(cern_dict)-puppet} are not managed"
         )
    cern_owner_dict = {}
    for key, value in cern_dict.items():
        owner = value['Owner']
        cern_owner_dict.setdefault(owner, []).append({key: value['Requests']})

    if email:
        email_counter = 1
        for email_to, values in cern_owner_dict.items():
            if override_to:
                email_to = override_to
            puppet_hosts = False
            tmp_host_list = []
            for item in values:
                key, v = next(iter(item.items()))
                tmp_host_list.append(f"{key}: {v} requests in the past {days} days")
                if cern_dict[key]['PuppetManaged']:
                    puppet_hosts = True
            hostlist_string = '\n'.join(tmp_host_list)
            if len(tmp_host_list) == 1:
                system_string = "system that has"
            else:
                system_string = "systems that have"
            if 'cc7' in ostag or 'rhel7' in ostag:
                # pylint: disable=line-too-long
                email_body = f"Dear {email_to},\n\nYou are receiving this email as you are the responsible owner of {len(tmp_host_list)} {system_string} accessed {distribution_full_name} ({distribution_short_name}) content from http://linuxsoft.cern.ch in the past {days} days.\n\nThis is a courtesy email to inform you that from the {eol_human}, {distribution_short_name} will be End of Life and support will cease. Also from this date, software updates will no longer be made available.\n\nCurrent users of {distribution_short_name} are strongly encouraged to migrate to RHEL or AlmaLinux 8/9. We remind you that the CERN Computing Rules ( OC5; https://cern.ch/ComputingRules ) require that you keep up-to-date all systems owned by you, in particular if those systems have approved openings in CERN's outer perimeter firewall.\n\nYou may find additional information about which distribution to choose from https://linux.web.cern.ch/which/\n\nFor additional information related to the end-of-life, please refer to the ITSSB entry https://cern.service-now.com/service-portal?id=outage&n=OTG0145248\n\nA full list of the systems that you are responsible for, and the number of requests made to linuxsoft.cern.ch can be seen below.\n\n"
            else:
                if datetime.strptime(eol, '%d.%m.%Y') > datetime.now():
                    cease_string = "will cease on the"
                    cease_extra = "From this date, t"
                    cease_extra2 = " will "
                    cease_extra3 = "would be"
                else:
                    cease_string = "ceased on the"
                    cease_extra = "T"
                    cease_extra2 = " "
                    cease_extra3 = "are"
                # pylint: disable=line-too-long
                email_body = f"Dear {email_to},\n\nYou are receiving this email as you are the responsible owner of {len(tmp_host_list)} {system_string} accessed {distribution_full_name} ({distribution_short_name}) content from http://linuxsoft.cern.ch in the past {days} days.\n\nWe would like to bring to your attention that support at CERN for {distribution_short_name} {cease_string} {eol_human}. {cease_extra}hese systems{cease_extra2}no longer receive system updates and {cease_extra3} in breach of the CERN Computing Rules ( OC5; https://cern.ch/ComputingRules ), which require that you keep up-to-date all systems owned by you, in particular if those systems have approved openings in CERN's outer perimeter firewall.\n\nPlease take action immediately to either decommission or migrate these systems to a CERN supported Linux distribution.\n\nYou may find currently supported distributions listed here: https://linux.web.cern.ch/which/\n\nA full list of the systems that you are responsible for, and the number of requests made to linuxsoft.cern.ch can be seen below\n\n"
            # If we have no puppet hosts, add a disclaimer
            if not puppet_hosts:
                # pylint: disable=line-too-long
                email_body = email_body + f"NOTE: The list of hosts below are hosts that have accessed {distribution_short_name} content from http://linuxsoft.cern.ch, this doesn't necessarily mean that the host is {distribution_short_name}. Please disregard this email if you are not running {distribution_short_name} systems\n\n"
            email_body = email_body + f"{hostlist_string}\n\n"
            # pylint: disable=line-too-long
            email_body = email_body + f"You may also consult the evolution of your {distribution_short_name} systems via the following interactive dashboard: {dashboard_url}&var-Department=All&var-OS={distribution_short_name.lower()}&var-Owner={email_to}\n\n---\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)"
            print(f"[{email_counter}/{len(cern_owner_dict)}]: sending an email to {email_to}")
            msg = MIMEMultipart()
            msg['Subject'] = f"[ACTION REQUIRED]: {distribution_full_name} ({distribution_short_name}) - End of Life"
            msg['From'] = email_from
            msg['To'] = email_to
            body = MIMEText(email_body)
            msg.attach(body)
            msg.add_header('reply-to', f"noreply.{email_from}")
            server = smtplib.SMTP('cernmx.cern.ch')
            try:
                server.sendmail(email_from, email_to, msg.as_string())
                # artificial sleep, we don't want to get blocked from sending
                # mail in bulk
                time.sleep(2)
            # Work out what this exception should be
            # pylint: disable=bare-except
            except:
                print(f"failed to send email to {email_to}, continuing...")
            email_counter += 1
    else:
        # at least print something
        print(
            f"RESULT: {len(cern_owner_dict)} email addresses would have been emailed"
            " if --email had of been passed"
        )
    if pretty:
        pprint.pprint(cern_owner_dict)

if __name__ == '__main__':
    sys.exit(main())
