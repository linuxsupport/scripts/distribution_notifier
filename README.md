# Distribution Notifier

This script provides the ability to notify users of a distribution via email. It has been historically used in the past to inform users that the system they are using is either end-of-life, or near end-of-life.

Data is retrieved from the opensearch index `linux_private-lxsoft-stats` that is populated by https://gitlab.cern.ch/linuxsupport/cronjobs/distributions-stats

## Operational details

* `--days` is used to control how many days worth of records to search
* `--ostag` is the ostag to search for
* No email will be sent without `--email` being passed, this allows for confirmation of data before sending a notification
* If `--email` is passed, 1 email per responsible will be sent which details a list of the systems they are responsible for and the total number of requests made by each system during the time period requested `--days`
* `--pretty` can be passed to provide details of the content that would be emailed
* Inclusions/Exclusions can be programmed with `--owner`, `--department`, `--group`, `--exclude_department`, `--type`
* `--onlydc` can be used to focus on systems that are housed in CERN datacentres
* `--emailfrom` can be used to change the default "From" email header (default: Linux.Support@cern.ch)
* `--override_to` can be used to override the "To" email header (useful for testing)
* `--maxrecords` can be used to set a limit on the returned records (useful for testing)
* `--debug` can be used to provide additional log info


## Miscellanous details
* If the host is puppet managed, the script queries puppetdb for the osmajor fact. If it's greater than the osmajor requested, the host is excluded (host upgraded)
* If a host has a more recent ostag than what was requested, the host is excluded (host upgraded)
* For --ostag cc7: ATS systems are excluded due to the ATS/IT CC7 ELS agreement

## Example usage

```
$ ./distribution_notifier.py --help
usage: distribution_notifier.py [-h] [--ostag OSTAG] [--days DAYS] [--maxrecords MAXRECORDS]
                                [--override_to OVERRIDE_TO] [--type HOSTTYPE] [--email]
                                [--owner OWNER [OWNER ...]] [--department DEPARTMENT [DEPARTMENT ...]]
                                [--group GROUP]
                                [--exclude_department EXCLUDE_DEPARTMENT [EXCLUDE_DEPARTMENT ...]]
                                [--onlydc] [--pretty] [--debug] [--emailfrom EMAIL_FROM]

Get statistics about CERN hosts accessing linuxsoft repositories

optional arguments:
  -h, --help            show this help message and exit
  --ostag OSTAG         OS tag to search for
  --days DAYS           number of days
  --maxrecords MAXRECORDS
                        optional maxrecords to return, useful for debugging
  --override_to OVERRIDE_TO
                        override the to email, useful for testing
  --type HOSTTYPE       query puppet,nonpuppet or all types of hosts
  --email               generate a template to send an email
  --owner OWNER [OWNER ...]
                        restrict query to one or more email addresses (whitespace separated)
  --department DEPARTMENT [DEPARTMENT ...]
                        restrict query to one or more specific departments (whitespace separated)
  --group GROUP         restrict query to a specific group (requires department)
  --exclude_department EXCLUDE_DEPARTMENT [EXCLUDE_DEPARTMENT ...]
                        a list of departments to exclude (whitespace separated)
  --onlydc              Only include systems that reside in a datacentre)
  --pretty              pretty print some additional stats
  --debug               print debug messages where appropriate
  --emailfrom EMAIL_FROM
                        If sending an email, who are we sending it as? default: Linux.Support@cern.ch
```

## Example output

```
$ ./distribution_notifier.py --ostag cc7 --days 7 --onlydc
INFO: running query with [ostag: cc7, days: 7, puppetmanaged: all, department: all, group: all, owner: all]
QUERY: 4547 raw total number of hosts, before exclusions applied
QUERY: excluded 717 ATS hosts
QUERY: excluded 121 hosts removed from DNS
QUERY: excluded 52 puppet hosts that have since upgraded
QUERY: excluded 16 unmanaged hosts that have since upgraded
RESULT: There are 3470 cc7 hosts that have accessed linuxsoft in the past 7 days
RESULT: Of which 2463 are puppet managed, and 1007 are not managed
RESULT: 588 email addresses would have been emailed if --email had of been passed
```

## Using the script to alert on deprecated operating systems

In May 2024, the script was used to alert users still using deprecated operating systems in CERN DCs. The command executed was the following (however with `--email` passed as well) :

```
$ for dist in slc6 c8 cs8 cs9; do echo "working on distribution $dist"; ./distribution_notifier.py --ostag $dist --days 7 --onlydc; done
working on distribution slc6
INFO: running query with [ostag: slc6, days: 7, puppetmanaged: all, department: all, group: all, owner: all]
QUERY: 26 raw total number of hosts, before exclusions applied
QUERY: excluded 0 hosts removed from DNS
QUERY: excluded 0 puppet hosts that have since upgraded
QUERY: excluded 0 unmanaged hosts that have since upgraded
RESULT: There are 26 slc6 hosts that have accessed linuxsoft in the past 7 days
RESULT: Of which 0 are puppet managed, and 26 are not managed
RESULT: 21 email addresses would have been emailed if --email had of been passed
working on distribution c8
INFO: running query with [ostag: c8, days: 7, puppetmanaged: all, department: all, group: all, owner: all]
QUERY: 77 raw total number of hosts, before exclusions applied
QUERY: excluded 0 hosts removed from DNS
QUERY: excluded 0 puppet hosts that have since upgraded
QUERY: excluded 1 unmanaged hosts that have since upgraded
RESULT: There are 76 c8 hosts that have accessed linuxsoft in the past 7 days
RESULT: Of which 0 are puppet managed, and 76 are not managed
RESULT: 53 email addresses would have been emailed if --email had of been passed
working on distribution cs8
INFO: running query with [ostag: cs8, days: 7, puppetmanaged: all, department: all, group: all, owner: all]
QUERY: 278 raw total number of hosts, before exclusions applied
QUERY: excluded 1 hosts removed from DNS
QUERY: excluded 0 puppet hosts that have since upgraded
QUERY: excluded 1 unmanaged hosts that have since upgraded
RESULT: There are 276 cs8 hosts that have accessed linuxsoft in the past 7 days
RESULT: Of which 38 are puppet managed, and 238 are not managed
RESULT: 157 email addresses would have been emailed if --email had of been passed
working on distribution cs9
INFO: running query with [ostag: cs9, days: 7, puppetmanaged: all, department: all, group: all, owner: all]
QUERY: 78 raw total number of hosts, before exclusions applied
QUERY: excluded 0 hosts removed from DNS
QUERY: excluded 0 puppet hosts that have since upgraded
QUERY: excluded 0 unmanaged hosts that have since upgraded
RESULT: There are 78 cs9 hosts that have accessed linuxsoft in the past 7 days
RESULT: Of which 0 are puppet managed, and 78 are not managed
RESULT: 49 email addresses would have been emailed if --email had of been passed
$
```
